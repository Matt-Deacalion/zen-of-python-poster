========================
The Zen of Python Poster
========================

A beautiful poster made to remind you of Tim Peters' renowned “Zen of Python”.
The guiding principles of a Pythonista. The `full colour version`_ looks stunning when printed on
A2 paper and framed. The `black and white version`_ still looks awesome and is optimized for black
and white printers.

+------------------------------------------------------+------------------------------------------------------+------------------------------------------------------+
| Full Colour                                          | Printed and Framed                                   | Black and White                                      |
+======================================================+======================================================+======================================================+
| .. image:: http://dirtymonkey.co.uk/pyposter-1.png?1 | .. image:: http://dirtymonkey.co.uk/pyposter-2.png?2 | .. image:: http://dirtymonkey.co.uk/pyposter-3.png?3 |
|     :alt: Zen of Python Poster - Full Colour         |     :alt: Zen of Python Poster - Printed and framed  |     :alt: Zen of Python Poster - Black and White     |
+------------------------------------------------------+------------------------------------------------------+------------------------------------------------------+

License
-------
Copyright © 2016 `Matt Deacalion Stevens`_, released under `this`_ Creative Commons License.

.. _full colour version: zen_poster.pdf?raw=True
.. _black and white version: zen_poster_white_dark_grey.pdf?raw=True
.. _Matt Deacalion Stevens: http://dirtymonkey.co.uk
.. _this: http://creativecommons.org/licenses/by-sa/4.0/
